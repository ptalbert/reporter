"""Utility functions like custom jinja filters."""


def status_to_emoji(status):
    """Convert test status to emoji."""
    # https://github.com/kernelci/kcidb-io/blob/main/kcidb_io/schema/v4.py#L508
    return {
        'PASS': '✅',
        'FAIL': '❌',
        'ERROR': '⚡',
        'SKIPPED': '⏭️'   # Extra case for the email subject
    }.get(status, '❔')


def yesno(condition, values="yes,no"):
    """Return first value if True, second if False, third if None."""
    # Mimics the django templates builtin filter
    # https://docs.djangoproject.com/en/3.0/ref/templates/builtins/#yesno
    values = values.split(',')
    if condition:
        return values[0]

    if condition is None and len(values) > 2:
        return values[2]

    return values[1]


def unique_test_descriptions(tests):
    """Return only unique combinations of a test architecuture, debug status and comment."""
    unique_tests = {}
    for test in tests:
        unique_attributes = (test.architecture, test.debug, test.comment)
        if unique_attributes not in unique_tests:
            unique_tests[unique_attributes] = test

    return list(unique_tests.values())


def join_addresses(addresses):
    """Join the addresses in a list together while stripping each."""
    return ', '.join(address.strip() for address in addresses)
