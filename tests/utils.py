"""Test utils."""
import json
from pkgutil import get_data


def load_json(filename):
    """Load json file from assets."""
    file_content = get_data(__name__, f'assets/{filename}')

    return json.loads(file_content)
